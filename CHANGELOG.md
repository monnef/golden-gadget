🌱 Change log 🌳
===============

🔸 0.6.0 🔸
===========

* Implemented `alt_arr`.
* Implemented `format_binary`.
* Implemented `from_RectangleShape2D_extents_to_Rect2`.
* Added output to debugger tab (`push_error`) from crash.
* Implemented point label in debug draw.
* Implemented `in_range` and `in_range_excl`.
* Implemented `rand_vec2_r`, added tests for `rand_vec2_r` and `rand_vec2`.
* `get_gpu_vram_mb` - refactoring, added nvidia-smi support on Linux and printing debug logs.
* Finally fixed `get_gpu_vram_mb` on Windows.
* Fixed `get_gpu_vram_mb` on Windows.
* Implemented (not very robust) `get_gpu_vram_mb`.
* Fixed `dehumanize_size` to return int instead of float without decimal
* Polishing of `dehumanize_size` - docs, tests, optimizations.
* Implemented `dehumanize_size`.
* Extended `sort_with` to support function-like mapper, not just FuncRef.
* Added `str_to_array` function.
* Added workaround in `fmt_input_event_key` to mitigate changes in Godot 3.5.
* Implemented `find_nodes`.
* Implemented `dir_files`, `drop_str` and `drop_right_str`.
* Fixed examples of `union` and `difference`.
* Renamed `vec2` to `vec2i` and `vec3` to `vec3i`, added float supporting `vec2` and `vec3`.
* Added rename option to `delete_children` function.
* Implemented `debounce`.
* Extracted `InputEventKey` formatting to its own function.
* Implemented `duplicate_deep`.
* Added docs and support of func-like to `flip`.

🔸 0.5.0 🔸
===========

* Implemented `shift_hue`.
* Implemented `draw_arrow`.
* Fixed `draw_ellipse` not respecting given center.
* DD2D - implemented `ellipse`, `ellipse_wire` and `arrow`.
* Implemented `union` and `difference` functions.
* Implemented `intersect`
* Implemented functions `debug` and `map_with_index`.
* Implemented `drop_while`, `drop_while_right` and `not_elem`.
* Implemented `valid_index` function.
* Added support for `InputEventMouseButton` to `input_event_to_dict`, `dict_to_input_event` and `fmt_input_event`.
* Implemented `get_nth_parent`.
* Added workaround in `fmt_input_event` for physical key bindings from editor.
* Implemented `fmt_input_event`.
* Implemented `pick`, `omit`, `assign_fields`, `input_event_to_dict` and `dict_to_input_event`.
* Implemented `flip` function.
* Polished more complex example of `DebugDraw2D` and added console output.
* Implemented `rect`, `rect_wire` and `string` in `DebugDraw2D`.
* Implemented test scene for `DebugDraw2D`.
* Implemented `elem`.
* Implemented `set_node_parent` (reparent).
* Implemented `debug_draw_2d` (point and line).
* Modified lambda generation to be in tool context.
* Implemented `zip_with_index`.
* Implemented `vec2` and `vec3`.
* Implemented `merge` function.
* Implemented `load_relative`.
* Fixed passing path as string to `get_node_or_crash`.
* Implemented option to skip tests which trigger spam bug in Godot.

🔸 0.4.0 🔸
===========
* Improved `quit` to enter debug mode when running from editor.
* Implemented `get_cmd_args` and `parse_cmd_args`.
* Implemented `format_time`.
* Implemented `assert_eq`.
* Implemented `rand_float` and `rand_int`.
* Removed string restriction on field name in `get_fld` and relatives.
* Implemented `average`, `min` and `max` functions.
* Implemented `find_by_fld_val_or_null` in GGA.
* Implemented `partition`, `is_GGArray` and `to_array_deep`.
* Improved `get_node_or_null` to support string and not spam error log.
* Implemented easy reading of JSON files (`read_json_file_or_error`, `read_json_file_or_null`, `read_json_file_or_crash`).
* Removed `F` from recommended imports, bumped version.

🔸 0.3.0 🔸
===========

* Changed `head` and `last` to crash on empty arrays.
* Implemented `find`, `find_or_null`, `head_or_null` and `last_or_null`.
* Added `rand_dir2`, `rand_dir3`, `rand_sign`, `rand_bool`, `dir_to2`, `dir_to3` and `clampi`.
* Fixed `new_array` to clone (duplicate) given value.
* Implemented `is_empty` in GG (main file).
* Implemented `group_with`, `transpose`, `nub` and `unique`.
* Renamed `flatten` to `flatten_raw` and `flatten_unwrapped` to `flatten` in GGA.
* Implemented `wrap` and `unwrap` in GGA.
* Updated docs for newer version of Gododoc.
* Implemented `take_while`.
* Implemented `snake_to_camel_case` and `decapitalize_first`.
* Implemented `capitalize_all`, `camel_to_snake_case`.
* Implemented `floats_are_equal`, `format_float_2`, `format_vec2_2` and `format_vec3_2`.
* Implemented `float_arr_to_int_arr`, `find_index_or_null` and `find_index`.
* Tests now exit with an error code on failure.
* Added error code parameter to `quit` function.

🔸 0.2.0 🔸
===========

* Added [real documentation](docs/index.md) in markdown.
  * Documented all functions (but not all types yet).
  * Added many examples.

<br/>

* Implemented `replicate`, `create_array` and `flow`.
* Fixed crash of debug prints in `pipe` when encountered `Array` as `pipe` input.
* Implemented `join_w` (GGA), exposed `join` (GG).
* Implemented `for_each` and non-returning lambdas.
* Implemented `get_fields` and `set_fields`.
* Renamed `f_bool` to `fmt_bool`.
* Implemented `bool` and `lazy_bool`.
* Fixed `is_empty` in GGA, added more tests.
* Implemented `get_children_rec`.
* `crash` and `assert` are now using `quit` instead of `assert`, terminating game when run without editor.
* Implemented `quit`.
* Implemented `pause_one` and `pause`.
* Implemented `get_node_or_null`.
* Implemented `generate_array` (variant of `new_array` with cell getter function).
* Renamed `create_array` to `new_array`.
* Implemented `const`.
