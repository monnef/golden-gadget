tool
extends Resource

class_name DebouncedF

var GGI

var _f
var _debounce_time: float
var _last_args: Array
var _in_progress:= false
var _remaining_time: float

func _init(f, debounce_time) -> void:
	_f = f
	_debounce_time = debounce_time

func call_func(args: Array) -> void:
	_last_args = args
	_in_progress = true
	_remaining_time = _debounce_time

func _call() -> void:
	GGI.call_spread_(_f, _last_args)

func process(delta: float) -> void:
	if !_in_progress: return
	_remaining_time -= delta
	if _remaining_time <= 0:
		_in_progress = false
		_call()
