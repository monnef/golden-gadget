tool
extends Node2D

class_name GGExecuter

var GGI = preload("GGInternal.gd").new()

var _functions:= []

func _process(delta: float) -> void:
	for f in _functions: f.process(delta)

func add_function(f):
	_functions.push_back(f)
	f.GGI = GGI

func remove_function(f): _functions.erase(f)
